<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Woofcreative_Commercebug_Block_Tab_Models extends Woofcreative_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_models.phtml');
    }		
}
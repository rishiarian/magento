<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Woofcreative_Commercebug_Block_Tab_Lookup extends Woofcreative_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_lookup.phtml');
    }		
}
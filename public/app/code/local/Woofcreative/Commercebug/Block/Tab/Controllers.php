<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Woofcreative_Commercebug_Block_Tab_Controllers extends Woofcreative_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_controllers.phtml');
    }	
}
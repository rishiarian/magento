<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Woofcreative_Commercebug_Model_System_Config_Source_Log_Format_Classes
{
    public function toOptionArray()
    {
        return array(
        'Woofcreative_Commercebug_Helper_Formatlog_Raw'=>'Raw JSON Notation',
        'Woofcreative_Commercebug_Helper_Formatlog_Raw'=>'All Information, Simple Text',
        'custom'=>'Custom Class'
        );
    }
}
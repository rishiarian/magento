<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Woofcreative_Commercebug_Model_Graphviz
{
    public function capture()
    {    
        $collector  = new Woofcreative_Commercebug_Model_Collectorgraphviz; 
        $o = new stdClass();
        $o->dot = Woofcreative_Commercebug_Model_Observer_Dot::renderGraph();
        $collector->collectInformation($o);
    }
    
    public function getShim()
    {
        $shim = Woofcreative_Commercebug_Model_Shim::getInstance();        
        return $shim;
    }    
}
<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
interface Woofcreative_Commercebug_Model_Interface_Ison
{
    public function isOn();
}